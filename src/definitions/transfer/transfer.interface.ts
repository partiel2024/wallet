import { IAccount } from "../accounts";

export interface ITransfer {
    to: string;
    amount: number;
    timestamp: Date;
    account: string | IAccount;
}
import { Router, Request, Response, json } from 'express';
import { Model, Mongoose } from 'mongoose';
import { ITransfer } from '../../definitions/transfer';
import { TransferSchema } from '../mongoose/schemas/transfer.schema';

export class Walletcontroller {

    private connection: Mongoose;
    private transferModel: Model<ITransfer>;

    public constructor(connection: Mongoose) {
        this.connection = connection;
        this.transferModel = this.connection.model("Transfer", TransferSchema);
    }

    buildRoutes(): Router {
        const router = Router();
        // router.post("/transfer", json(), this.transfer.bind(this));
        // router.get("/getAllTransfers", this.getAllTransfers.bind(this));
        // router.get("/getBalance", this.getBalance.bind(this));
        return router;
    }

}
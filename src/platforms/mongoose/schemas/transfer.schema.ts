import mongoose, { SchemaTypes } from "mongoose";
import { ITransfer } from "../../../definitions/transfer";

export const TransferSchema = new mongoose.Schema<ITransfer>({
    to: {
        type: SchemaTypes.String,
        required: true
    },
    amount: {
        type: SchemaTypes.Number,
        required: true
    },
    timestamp: {
        type: SchemaTypes.Date,
        required: true
    },
    account: {
        type: SchemaTypes.ObjectId,
        required: true,
        ref: "Account"
    }
}, {
    versionKey: false,
    collection: "transfers",
    timestamps: true
});